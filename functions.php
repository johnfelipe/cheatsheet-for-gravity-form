<?php
/**
 * Deep Theme.
 * Depp definitions and call main file.
 *
 * @since   1.0.0
 * @author  Webnus
 */

// Don't load directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DEEP_VERSION', '3.2.2' );

/**
 * Core path.
 *
 * @since 1.0.0
 */
define( 'DEEP_DIR', get_template_directory() . '/' );
define( 'DEEP_URL', get_template_directory_uri() );
define( 'DEEP_ASSETS_URL', DEEP_URL . '/assets/dist/' );
define( 'DEEP_ASSETS_DIR', DEEP_DIR . 'assets/dist/' );
define( 'DEEP_INCLUDES_DIR', DEEP_DIR . 'inc/' );
define( 'DEEP_CORE_DIR', DEEP_DIR . 'inc/core/' );
define( 'DEEP_CORE_URL', DEEP_URL . '/inc/core/' );
define( 'DEEP_SVG', '<svg version="1.1" id="Rectangle_3_1_" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="562 242 76 76" style="enable-background:new 562 242 76 76;" xml:space="preserve"><g id="Rectangle_3"><g><linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="570.627" y1="322.1832" x2="616.2863" y2="246.1934" gradientTransform="matrix(1 0 0 -1 0 560)"><stop  offset="0" style="stop-color:#4400D0"/><stop  offset="0.43" style="stop-color:#6520F4"/><stop  offset="1" style="stop-color:#930AFD"/></linearGradient><path class="st0" d="M599.8,242.7h-30.6h-1.1h-5.6v60.8h5.6h1.1h30.6c13.1,0,23.9-10.4,23.9-23.9s-10.4-23.9-23.9-23.9h-23.9v2.6 v4.5V290h1.9h5.2h17.2c5.6,0,10.1-4.5,10.1-10.1s-4.5-10.1-10.1-10.1h-10.4v6.7h10.4c1.9,0,3.4,1.5,3.4,3.4c0,1.9-1.5,3.4-3.4,3.4 H583v-20.1h17.2c9.3,0,16.8,7.5,16.8,16.8s-7.5,16.8-16.8,16.8h-30.6v-47.4h30.6c16.8,0,30.6,13.8,30.6,30.6s-13.8,30.6-30.6,30.6 h-23.9v6.7h23.9c20.5,0,37.3-16.8,37.3-37.3S620.3,242.7,599.8,242.7z"/></g></g></svg>' );

require_once DEEP_INCLUDES_DIR . 'init.php';
require_once DEEP_INCLUDES_DIR . 'core/deepcore.php';
require_once DEEP_INCLUDES_DIR . 'main.php';

function clean($string) {
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
add_action( 'gform_after_submission', 'access_entry_via_field', 10, 2 );
function access_entry_via_field( $entry, $form ) {
     
     foreach( $form['fields'] as $k=>&$a) {

        if( $a->label == 'Bloque HTML')

            unset($form['fields'][$k]);

        if( $a->label == 'Radicado')

            unset($form['fields'][$k]);

        if( $a->label == '')

            unset($form['fields'][$k]);
    }

     
    $string = str_replace(' ', '_', $form['title']); 
    $tablename = preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
    $table_name = $wpdb->prefix.$tablename;    
    $labels=array();
   
    foreach ( $form['fields'] as $field ) {
        
            global $wpdb; 

                    $query = "CREATE TABLE `$table_name` ( ";
                    $query.= "id MEDIUMINT NOT NULL AUTO_INCREMENT,";
                    foreach ( $form['fields'] as $field )
                    {
                      
                        $res=clean($field->label);

                        //echo  $res .'<br>';
                        $values = rgar( $entry, (string) $field->id );
                        $labels[$res]=$values;    
                           
                            $max_length = 64;


                                if (strlen($res) > $max_length)
                                {
                                    $offset = ($max_length - 3) - strlen($res);
                                    $res = substr($res, 0, strrpos($res, ' ', $offset));
                                }
                               

                               if($res!='')
                               {
                                 
                                     $query .= "$res" . " " . "varchar(255) NOT NULL,";
                                  
                                 
                               }
                    }
            
                    $query.="PRIMARY KEY  (id)";
                    $query .= " ); ";
                    echo $query .'<br>';
                    
                    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
                        $wpdb->query($query);
                        print_r($wpdb->last_error);
              
                    } 
                   

                
            }
  
        if($table_name == 'InformacinGeneral') 
        {
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAlcantarillado']);
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAcueducto']);
        }
        if($table_name == 'FormatodediagnsticoJurdicoLnea1') 
        {
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAcueducto']);
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAlcantarillado']);
            unset($labels['CuentanconelCertificadodeExistenciayRepresentacinLegalMtododeverificacin']);
            unset($labels['CuentanconcontratoconcesinoperacinsimilarsuscritoconlosprestadoresdeserviciosdeacueductoalcantarilladoyaseoCumple']);
            unset($labels['CuentanconcontratoconcesinoperacinsimilarsuscritoconlosprestadoresdeserviciosdeacueductoalcantarilladoyaseoMtododeverificacin']);
            unset($labels['CuentanconcontratoconcesinoperacinsimilarsuscritoconlosprestadoresdeserviciosdeacueductoalcantarilladoyaseoObservacin']);
            unset($labels['CuentanconlosestatutosdelserviciodealcantarilladoMtododeverificacin']);
            unset($labels['CuentanconlaescriturapblicadeconstitucindelaasociacinMtododeverificacin']);
            unset($labels['CuentanconelRegistronicodePrestadoresdeServiciosPblicos-RUPSCumple']);
            unset($labels['CuentanconelRegistronicodePrestadoresdeServiciosPblicos-RUPSMtododeverificacin']);
            unset($labels['CuentanconelRegistronicodePrestadoresdeServiciosPblicos-RUPSObservacin']);
            unset($labels['CuentanconlaconstanciadeagotamientodelArtculo6delaLey142de1994Cumple']);
            unset($labels['CuentanconlaconstanciadeagotamientodelArtculo6delaLey142de1994Mtododeverificacin']);
            unset($labels['CuentanconlaconstanciadeagotamientodelArtculo6delaLey142de1994Observacin']);
            unset($labels['CuentanconcontratosdeobrapblicafinanciadosconrecursosdelSGP-APSBCumple']);
            unset($labels['CuentanconcontratosdeobrapblicafinanciadosconrecursosdelSGP-APSBMtododeverificacin']);
            unset($labels['CuentanconcontratosdeobrapblicafinanciadosconrecursosdelSGP-APSBObservacin']);
            unset($labels['CuentanconcontratosdeconsultorasfinanciadosconrecursosdelSGP-APSBCumple']);
            unset($labels['CuentanconcontratosdeconsultorasfinanciadosconrecursosdelSGP-APSBMtododeverificacin']);
            unset($labels['CuentanconcontratosdeconsultorasfinanciadosconrecursosdelSGP-APSBObservacin']);
            unset($labels['ElMunicipiocuentaconloscontratosdeconcesionesvigentesMtododeverificacin']);
            unset($labels['EnlaactualidadexistencontratosverbalesconfuncionariosdelaentidadCumple']);
            unset($labels['EnlaactualidadexistencontratosverbalesconfuncionariosdelaentidadMtododeverificacin']);
            unset($labels['EnlaactualidadexistencontratosverbalesconfuncionariosdelaentidadObservacin']);
            unset($labels['CuentaconcontratossuscritosconotrasentidadessubcontrataserviciosconotrasempresasCumple']);
            unset($labels['CuentaconcontratossuscritosconotrasentidadessubcontrataserviciosconotrasempresasMtododeverificacin']);
            unset($labels['CuentaconcontratossuscritosconotrasentidadessubcontrataserviciosconotrasempresasObservacin']);
            unset($labels['CuentanconlospermisosambientalesysanitariosrequeridosMtododeverificacin']);
            unset($labels['CuentanconpermisosmunicipalesparaeldesarrollodelasobrasdeinfraestructuraCumple']);
            unset($labels['CuentanconpermisosmunicipalesparaeldesarrollodelasobrasdeinfraestructuraMtododeverificacin']);
            unset($labels['CuentanconpermisosmunicipalesparaeldesarrollodelasobrasdeinfraestructuraObservacin']);
            unset($labels['RealizaronexpropiacionesparalaprestacindelservicioMtododeverificacin']);
            unset($labels['ElMunicipiocuentaconmanualdesupervisineinterventoracuandolasempresasson100privadasCumple']);
            unset($labels['ElMunicipiocuentaconmanualdesupervisineinterventoracuandolasempresasson100privadasMtododeverificacin']);
            unset($labels['ElMunicipiocuentaconmanualdesupervisineinterventoracuandolasempresasson100privadasObservacin']);
            unset($labels['CuentanconuncomitdeestratificacinparalastarifasdelmunicipioCumple']);
            unset($labels['CuentanconuncomitdeestratificacinparalastarifasdelmunicipioMtododeverificacin']);
            unset($labels['CuentanconuncomitdeestratificacinparalastarifasdelmunicipioObservacin']);
            unset($labels['EnlaactualidadlaempresacuentaconprocesosadministrativossancionatoriosvigentesCumple']);
            unset($labels['EnlaactualidadlaempresacuentaconprocesosadministrativossancionatoriosvigentesMtododeverificacin']);
            unset($labels['EnlaactualidadlaempresacuentaconprocesosadministrativossancionatoriosvigentesObservacin']);
            unset($labels['SielmunicipioesprestadordirectodelosservicioslaestructuradeladependenciaqueseencargadelaprestacindelosserviciosydelasfuncionesasignadasacadamunicipioCumple']);
            unset($labels['SielmunicipioesprestadordirectodelosservicioslaestructuradeladependenciaqueseencargadelaprestacindelosserviciosydelasfuncionesasignadasacadamunicipioMtododeverificacin']);
            unset($labels['SielmunicipioesprestadordirectodelosservicioslaestructuradeladependenciaqueseencargadelaprestacindelosserviciosydelasfuncionesasignadasacadamunicipioObservacin']);
            unset($labels['EnlaactualidadlaempresacuentaconprocesosjudicialesvigentesMtododeverificacin']);
            unset($labels['EnlaactualidadlaempresacuentaconprocesosjudicialesvigentesObservacin']);
            unset($labels['CuentanconcontrolparadarrespuestasalasPQRSdentrodelostrminosestablecidosporlaleyCumple']);
            unset($labels['CuentanconcontrolparadarrespuestasalasPQRSdentrodelostrminosestablecidosporlaleyMtododeverificacin']);
            unset($labels['CuentanconcontrolparadarrespuestasalasPQRSdentrodelostrminosestablecidosporlaleyObservacin']);
            unset($labels['CuentanconacuerdomunicipalenelqueseestablecelosfactoresdecontribucionesysubsidiosCumple']);
            unset($labels['CuentanconacuerdomunicipalenelqueseestablecelosfactoresdecontribucionesysubsidiosMtododeverificacin']);
            unset($labels['CuentanconacuerdomunicipalenelqueseestablecelosfactoresdecontribucionesysubsidiosObservacin']);
            unset($labels['CuentanconconveniodetransferenciasubsidiossuscritosentrelaempresayoperadordelmunicipioCumple']);
            unset($labels['CuentanconconveniodetransferenciasubsidiossuscritosentrelaempresayoperadordelmunicipioMtododeverificacin']);
            unset($labels['CuentanconconveniodetransferenciasubsidiossuscritosentrelaempresayoperadordelmunicipioObservacin']);
        }
        if($table_name == 'FormatodeDiagnosticoTcnicoLinea1') 
        {
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAcueducto']);
            unset($labels['NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAlcantarillado']);
            unset($labels['ExistenactividadesantrpicasaguasarribadelabocatomaMtododeverificacin']);
            unset($labels['EnlaplantadetratamientoserealizaelprocesodedesinfeccinMtododeverificacin']);
            unset($labels['LaplantadetratamientocuentacontanquesdealmacenamientoMtododeverificacin']);
            unset($labels['EnlaplantadetratamientosecuentaconunsistemademanejodelodosMtododeverificacin']);
            unset($labels['EnlaplantadetratamientosecuentaconunsistemademanejodelodosObservacin']);
            unset($labels['LaplantadetratamientocuentaconmanualdeoperacionesMtododeverificacin']);
            unset($labels['SerealizamedicindeparametrosdecalidaddelaguaenlaPTAPMtododeverificacin']);
            unset($labels['SecuentaconPlandeusoeficienteyahorrodelAguaPUEAAMtododeverificacin']);
            unset($labels['SeencuentranactualizadoslosplanosderedesdecaptacinconduccinplantadetratamientoytanquesdealmacenamientoCumple']);
            unset($labels['SeencuentranactualizadoslosplanosderedesdecaptacinconduccinplantadetratamientoytanquesdealmacenamientoMtododeverificacin']);
            unset($labels['SeencuentranactualizadoslosplanosderedesdecaptacinconduccinplantadetratamientoytanquesdealmacenamientoObservacin']);
            unset($labels['LosoperariosdelaplantadetratamientoseencuentrancapacitadosMtododeverificacin']);
            unset($labels['LosoperariosdelaplantadetratamientoseencuentrancapacitadosObservacin']);
            unset($labels['SellevanacabomantenimientospreventivosycorrectivosalsistemadealcantarilladoCumple']);
            unset($labels['SellevanacabomantenimientospreventivosycorrectivosalsistemadealcantarilladoMtododeverificacin']);
            unset($labels['SellevanacabomantenimientospreventivosycorrectivosalsistemadealcantarilladoObservacin']);
            unset($labels['SecuentaconplandesaneamientoymanejodevertimientosPSMVMtododeverificacin']);
            unset($labels['SecuentaconplantadetratamientodeaguasresidualesMtododeverificacin']);
            unset($labels['LaplantadetratamientodeaguasresidualescuentacondesarenadoresosedimentadoresCumple']);
            unset($labels['LaplantadetratamientodeaguasresidualescuentacondesarenadoresosedimentadoresMtododeverificacin']);
            unset($labels['LaplantadetratamientodeaguasresidualescuentacondesarenadoresosedimentadoresObservacin']);
            unset($labels['LaplantadetratamientocuentaconreactoresbiolgicosMtododeverificacin']);
            unset($labels['SecuentaconotrotipodesistemadetratamientodeaguasresidualesdiferentealasconvecionalesCumple']);
            unset($labels['SecuentaconotrotipodesistemadetratamientodeaguasresidualesdiferentealasconvecionalesMtododeverificacin']);
            unset($labels['SecuentaconotrotipodesistemadetratamientodeaguasresidualesdiferentealasconvencionalesObservacin']);
            unset($labels['SecuentaconpermisodevertimientosdeaguasresidualesMtododeverificacin']);
            unset($labels['LosoperariosdelaplantadetratamientodeaguasresidualesseencuentrancapacitadosCumple']);
            unset($labels['LosoperariosdelaplantadetratamientodeaguasresidualesseencuentrancapacitadosMtododeverificacin']);
            unset($labels['LosoperariosdelaplantadetratamientodeaguasresidualesseencuentrancapacitadosObservacin']);
            unset($labels['SecuentaconmanualdeoperacindelaplantadetratamientoMtododeverificacin']);
            unset($labels['SecuentaconunplandecontingenciasparaelmanejodedesastresyemergenciasCumple']);
            unset($labels['SecuentaconunplandecontingenciasparaelmanejodedesastresyemergenciasMtododeverificacin']);
            unset($labels['SecuentaconunplandecontingenciasparaelmanejodedesastresyemergenciasObservacin']);
            unset($labels['LaempresaprestaelservicioderecoleccindomiciliariaderesiduosslidosCumple']);
            unset($labels['LaempresaprestaelservicioderecoleccindomiciliariaderesiduosslidosMtododeverificacin']);
            unset($labels['LaempresaprestaelservicioderecoleccindomiciliariaderesiduosslidosNotas']);
            unset($labels['LaempresaprestaelservicioderecoleccindomiciliariaderesiduosslidosObservacin']);
            unset($labels['CuentanconvehculoscompactadoresparalarecoleccinderesiduosMtododeverificacin']);
            unset($labels['CuentanconvehculoscompactadoresparalarecoleccinderesiduosObservacin']);
            unset($labels['SecuentaconprogramasdereciclajeoasociacionesderecicladoresMtododeverificacin']);
            unset($labels['SecuentaconprogramasdereciclajeoasociacionesderecicladoresObservacin']);
            unset($labels['SubcontratanlarecoleccindeRESPELconalgunaempresagestoraautorizadaCumple']);
            unset($labels['SubcontratanlarecoleccindeRESPELconalgunaempresagestoraautorizadaMtododeverificacin']);
            unset($labels['SubcontratanlarecoleccindeRESPELconalgunaempresagestoraautorizadaObservacin']);
            unset($labels['SecuentaconalgnplandecontingenciasparaelmanejodedesastresyemergenciasCumple']);
            unset($labels['SecuentaconalgnplandecontingenciasparaelmanejodedesastresyemergenciasMtododeverificacin']);
            unset($labels['SecuentaconalgnplandecontingenciasparaelmanejodedesastresyemergenciasNotas']);
            unset($labels['SecuentaconalgnplandecontingenciasparaelmanejodedesastresyemergenciasObservacin']);
            unset($labels['LadisposicinfinaldelosresiduosslidosessubcontradadaMtododeverificacin']);
            unset($labels['ElrellenocumpleconlosretirosestablecidosparafuenteshidiricasyviviendasCumple']);
            unset($labels['ElrellenocumpleconlosretirosestablecidosparafuenteshidiricasyviviendasMtododeverificacin']);
            unset($labels['ElrellenocumpleconlosretirosestablecidosparafuenteshidiricasyviviendasObservacin']);
            unset($labels['SecuentaconcompactadorymaquinariaamarilladentrodelrellenosanitarioCumple']);
            unset($labels['SecuentaconcompactadorymaquinariaamarilladentrodelrellenosanitarioMtododeverificacin']);
            unset($labels['SecuentaconcompactadorymaquinariaamarilladentrodelrellenosanitarioObservacin']);
            unset($labels['ElrellenosanitariocuentaconbsculaparaelpesajedelosresiduosMtododeverificacin']);
            unset($labels['ElrellenosanitariocuentaconbsculaparaelpesajedelosresiduosObservacin']);
            unset($labels['LosoperariosdelrellenosanitarioseencuentrancapacitadosMtododeverificacin']);
        }

     
        foreach($labels as $key =>$value)
        {
            
            
             $max_length = 64;
                                
            if (strlen($key) > $max_length)
            {
                $offset = ($max_length - 3) - strlen($key);
                $key = substr($key, 0, strrpos($key, ' ', $offset));


            }
             $capture_field .= '"'.$key.'"'.",";
            $capture_field_vals .='"'.$value.'"'."," ;

            
        }
       
        $gravityvalues=substr($capture_field_vals, 0, -1);
    
         if($table_name == 'InformacinGeneral') 
         {
            $sql="INSERT INTO `InformacinGeneral` (`Departamentodeatencin`, `Municipiodeatencin`, `PDA`, `TipodeLocalidad`, `NombredelaLocalidad`, `Nmerototaldedomiciliosenlalocalidad`, `Tipodeentidadprestadora`, `Nombre`, `Direccin`, `TelfonoFax`, `Departamentodedomicilio`, `Municipiodedomicilio`, `NIT`, `Fechadeconstitucin`, `Representantelegal`, `Cargo`, `Correo`, `RUPS`, `NmerodeempleadosadministrativosAcueducto`, `NmerodeoperariosGeneralesAcueducto`, `NmerodecontratistasAcueducto`, `TotalnmerodeempleadosAcueducto`, `NmerodeempleadosadministrativosAlcantarillado`, `NmerodeoperariosGeneralesAlcantarillado`, `NmerodecontratistasAlcantarillado`, `TotalnmerodeempleadosAlcantarillado`, `NmerodeempleadosadministrativosAseo`, `NmerodeoperariosGeneralesAseo`, `NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAseo`, `NmerodecontratistasAseo`,`TotalnmerodeempleadosAseo`) VALUES (".$gravityvalues.")";
            
        }
        else if($table_name == 'FormatodediagnsticoAdministrativoLnea1')
        {
            $sql="INSERT INTO `AdministrativeDiagnosticLine1` (`GuestDod`, `GuestNumber`, `PDA`, `Localitytype`, `LocalityName`, `Nvarototalomicomicrosinthelocality`, `Typeofreceiverentity`, `Name`, `Address`, `TelephoneFax`, `Departmentofomic`, `Municipalityedomicile`, `NIT`, `Dateconstitution`, `Legalrepresentative`, `Position`, `Mail`, `RUPS`, `NumberofadministrativeemployeesArcaduct`, `NumericDoctorsGeneralAnald`, `NumberofcontractorsA`, `TotalNumberofemployeesAvault`, `NumberofemployeesadministrativesWalkingsewer`, `NumberofoperatorsGeneralSetting`, `NumberofcontractorsWalkingsewer`, `TotalnumberofemployeesSewersewer`, `NumberofadministrativeemployeesToilet`, `NumberofemployeesGeneral`, `Numberofusersofmunicipalwaterresourcesandslaughtertoilet`, `Numberofcontractorstoilet`, `Totalnumberofemployeestoilet`, `IthasbeenelaboratedandpublishedtheinternalworkregulationCode`, `TheinternalworkingorderregulationNote`, `SehaelaboradoeimplementadoelmanualdefuncionesCumple`, `SehaelaboradoeimplementadoelmanualdefuncionesMtododeverificacin`, `SehaelaboradoeimplementadoelmanualdefuncionesObservacin`, `SehaelaboradoeimplementadoelmanualdeprocedimientosCumple`, `SehaelaboradoeimplementadoelmanualdeprocedimientosObservacin`, `SehandefinidoeimplementadolosprocesosdeseleccindepersonalCumple`, `SehadiseadouncursodeinduccinparaelpersonalnuevoCumple`, `Aninductioncourseforthenewstaffhasbeenobserved`, `AmechanismforthestaffassessmenthasbeensetCumple`, `ThecompanyhasastaffingknowledgeCalmple`, `LaempresatieneconocimientodelarotacindepersonalObservacin`, `SehadefinidounplandecapacitacionesinstitucionalesCumple`, `SehadefinidounplandecapacitacionesinstitucionalesObservacin`, `SehaelaboradounprogramadebienestarsocialCumple`, `SehaelaboradounprogramadebienestarsocialMtododeverificacin`, `SehaelaboradounprogramadebienestarsocialObservacin`, `SeharealizadomedicindeclimalaboralCumple`, `WorkMedicalMedicityProgramme`, `WorkMedicineMedicalObserver`, `AkeyforSSSSTCimplementation`, `AkeyforSimplementationforSGSSTOVarotation`, `CuentaconunaPoliticaenSSTCumple`, `CuentaconunaPoliticaenSSTMtododeverificacin`, `CuentaconunaPoliticaenSSTObservacin`, `CuentaconunplandetrabajoanualenSSTCumple`, `CuentaconunplandetrabajoanualenSSTMtododeverificacin`, `CuentaconunplandetrabajoanualenSSTObservacin`, `TieneconformadoelCCLComitdeConvivenciaLaboralCumple`, `IthasconformedtotheLCLCconvitionalityLaboralVerificationmethod`, `HasconformedtheCLCcontentofLaboralObservation`, `AbillchargingprocedurehasbeendefinedCumple`, `AbillchargingprocedureReview`, `SehaimplementadounprocedimientoparalarevisinpreviaCumple`, `SehaimplementadounprocedimientoparalarevisinpreviaObservacin`, `LaentidadcuentaconunprogramadeusoeficienteyahorrodelaguaCumple`, `SepresentacontabilidadseparadaporlosserviciosqueseprestanCumple`, `SeestanaplicandoprocedimientosdeTesoreriagilesyeficientesCumple`, `LaentidadtieneunregistroactualizadodelacarteraporedadesCumple`, `LaentidadseencuentraimplementadolasNIIFCumple`, `TheentityisenrolledintheNIFIFMethodofverifying`, `TheentityisenrolledinNIFIFObservation`, `TheMunicipalitycompriseswiththetradingcompletecommandCumple`, `ElMunicipiocuentaconelcomitpermanentedeestratificacinObservacin`, `LaentidadtienedefinidounplanestratgicoCumple`, `LaentidadtienedefinidounplanestratgicoMtododeverificacin`, `LaentidadtienedefinidounplanestratgicoObservacin`, `SehadeterminadolaestructuraorganicadelaempresaCumple`, `SehadeterminadolaestructuraorganicadelaempresaMtododeverificacin`, `SehadeterminadolaestructuraorganicadelaempresaObservacin`, `TheentityislocatedwiththereportstotheSUCUumple`, `TheentityislocatedwiththereportstotheSUTVallverifyingvalue`, `TheentityislocatedwiththereportstotheUUobserving`, `TheentityhasanannualpurchaseplanCovers`, `LaentidadcuentaconunplananualdecomprasMtododeverificacin`, `LaentidadcuentaconunplananualdecomprasObservacin`, `CuentaconlistadodepreciosyproveedoresactualizadoCumple`, `CuentaconlistadodepreciosyproveedoresactualizadoObservacin`, `LaentidaddisponedeuninventarioactualizadoCumple`, `LaentidaddisponedeuninventarioactualizadoMtododeverificacin`, `LaentidaddisponedeuninventarioactualizadoObservacin`, `TheassetsareclearlyidentifiedCumple`, `Theassetsareidentifiedidentily`, `Theindividualsidentifiedvarchar`, `ThefedentedpersonalitywithinternalcontrolmanualCumple`, `LaentidadprestadoracuentaconmanualdecontrolinternoObservacin`, `LaentidadcuentaconunaoficinadeatencinalusuarioCumple`, `LaentidadcuentaconunaoficinadeatencinalusuarioMtododeverificacin`, `LaentidadcuentaconunaoficinadeatencinalusuarioObservacin`, `LaentidadcuentaconunregistroactualizadodeatencinaPQRSCumple`, `LaentidadcuentaconunregistroactualizadodeatencinaPQRSObservacin`, `SetieneconstituidoelcomitdedesarrolloycontrolsocialCumple`, `Thecontrolleddevelopmentcommandhasbeenconstituted`, `TheNativeAccountwithaPAGIRSCumple`, `TheentityhasaPAGIRSVerificationmethod`, `TheentityhasaPAGIRobservation`, `NumberreceptedcontrollersAnalysis`, `NUMBERreceptorcontrolsdisplayedVarcharsewer`, `NUMBERreceptorcontrolsdisplayedVarchar`, `STANDARDNAME`, `STANDARDNUMBEREDUCTSsewer`, `STANDARDEDNUMBERStoilet`, `NumerousubscribersAvacher`, `Numberofsubscriberssewer`, `Numberofsubscribers`, `Numberofrecordsbooklet`, `NumberofrecordsSewerw`, `Numberofwidgets`, `Numberofselectedrecordsincollectionandtransportbooklet`, `Nominallycomedatedcollectionandtransportvarchar`, `NominallycollectednumberincollectionandtransportVarchar`, `TotaledomicilesVarcharduck`, `TotaledomicilesWilcharsewer`, `TotaledomicilesWar`, `NocertifiedoperatorsSENAA`, `NocertifiedoperatorsSENAWalkingsewer`, `NumberofcertifiedoperatorsSENAAseo`, `NumberofbusinessexecutivesAcompany`, `Numberofbusinessmenofthesewer`, `Numberofbusinessmenofthecompany`, `NumberofnewcontractsAccount`, `NumberofnewcontactsWilcharsewer`, `NumberofnewcontactsBathroom`, `TotalnmerocontratosdepersonalAcueducto`, `TotalnmerocontratosdepersonalAlcantarillado`, `TotalnmerocontratosdepersonalAseo`, `CuentasporcobrarasectoroficialyaparticularesAcueducto`, `CuentasporcobrarasectoroficialyaparticularesAlcantarillado`, `CuentasporcobrarasectoroficialyaparticularesAseo`, `ValorfacturadoasectoroficialyparticularesAcueducto`, `ValueNavailableandSpecialSectorStandard`, `NomercatedValuefactorypropertyvarchar`, `NumberofnewusersAvault`, `NumberofnewusersServing`, `NumberofnewusersShield`, `NumberofusernamestendedA`, `UserNumberSettingsvarchar`, `NumberofusersetsKathroom`, `Volumeproducedinvoicevolume`, `VolumeproducedinvoiceRequirer`, `VolumeproducedVolumenfacturadoWar`, `VolumenproducidoAducado`, `VolumeproducedStarter`, `VolumeproducedVarchar`, `Totalgeneralsaccount`, `TotalgeneraltiesSetting`, `TotalgeneralsageStarage`, `RevenueforserviceAvault`, `RevenuefromserviceprovisionSewersystem`, `Revenuefromserviceprovision`, `TotaloperatingcostsA`, `TotalOperatingCountriesWarcharsewer`, `TotalOperatingCountries`, `ServiceRepublicInput`, `ServiceRepublicInputService`, `OperationalUtilitiesArcache`, `OperationalUtilitiesWalkingSewer`, `OperationalServices`, `OperationalAvaducts`, `OperationalAssimilar`, `OperationalAvanced`, `UsedBacksaversActors`, `UsedBacksaversBackers`, `UsedbyinvestmentattestersArchive`, `InputorsalesArcaduct`, `SalesInputWarning`, `EnterorValue`, `TotalValueAvault`, `TotalpassVallar`, `PassivototalWash`, `ActivototalArcaduct`, `ActivototalAlcantarillado`, `ActivototalAvail`, `FinancialexpensesAvault`, `FinancialexpensesSilversewer`, `FinancialexpensesAvail`, `Invoicednotnotworkedprovidedtheservicesprovided`, `InputnotworkednotallservicesprovidedService`, `RetrievalvalueofendvalueAduck`, `RetrievalvalueofendvalueStack`, `Retrievalvalueofendvalue`, `Endvaluedatavaluevalue`, `valuefileSummaryValue`, `ValueNumberSuidValue`, `ReversalValchar`, `ReversalValcharged`, `ReversalValue`, `ReversedInvestredA`, `BudgetInvestment`, `BudgetInvestredsear`, `Investmentinfinancedinvestment`, `Investmentinfinancedinternalseat`, `Investmentinfinancedinternalseed`, `Invertintotal`, `InvertintotalSeatting`, `InvertintotalWash`, `NumberofPQRSatendingsAqueduct`, `NmerodePQRSatendidasAlcantarillado`, `NmerodePQRSatendidasAseo`, `NmerodePQRSrecibidasAcueducto`, `NmerodePQRSrecibidasAlcantarillado`, `NmerodePQRSrecibidasAseo`, `TiempoderespuestapromedioPQRSatendidasAcueducto`, `TiempoderespuestapromedioPQRSatendidasAlcantarillado`, `AverageresponsetimePQRSatendingsAvail`, `TimeupputPQRSAque`, `TimeupputPQRSAsewer`, `MaximumtimeputPQRSAse`, `NumberkeywordsreceivedforinvoiceAqueduct`, `NumberofclaimsreceivedbyinvoiceSettlement`, `NumberofreceiptsreceivedforinvoiceCleaning`, `NummagazineeditAvache`, `NummagazinesemittingWalkingsewer`, `NummagazinesemittedStair`) VALUES (".$gravityvalues.")";
        }
        else if($table_name == 'FormatodediagnsticoJurdicoLnea1'){
            echo $sql="INSERT INTO `FormatodediagnsticoJurdicoLnea1` (`Departamentodeatencin`, `Municipiodeatencin`, `PDA`, `Tipodelocalidad`, `Nombredelalocalidad`, `Nmerototaldedomiciliosenlalocalidad`, `Tipodeentidadprestadora`, `Nombre`, `Direccin`, `TelfonoFax`, `Departamentodedomicilio`, `Municipiodedomicilio`, `NIT`, `Fechadeconstitucin`, `Representantelegal`, `Cargo`, `Correo`, `RUPS`, `NmerodeempleadosadministrativosAcueducto`, `NmerodeoperariosGeneralesAcueducto`, `NmerodecontratistasAcueducto`, `TotalempleadosAcueducto`, `NmerodeempleadosadministrativosAlcantarillado`, `NmerodeoperariosGeneralesAlcantarillado`, `NmerodecontratistasAlcantarillado`, `TotalempleadosAlcantarillado`, `NmerodeempleadosadministrativosAseo`, `NmerodeoperariosGeneralesAseo`, `NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAseo`, `NmerodecontratistasAseo`, `TotalempleadosAseo`, `CuentanconelCertificadodeExistenciayRepresentacinLegalCumple`, `CuentanconelCertificadodeExistenciayRepresentacinLegalObservacin`, `CuentanconlosestatutosdelserviciodeacueductoCumple`, `CuentanconlosestatutosdelserviciodeacueductoMtododeverificacin`, `CuentanconlosestatutosdelserviciodeacueductoObservacin`, `CuentanconlosestatutosdelserviciodeaseoCumple`, `CuentanconlosestatutosdelserviciodeaseoMtododeverificacin`, `CuentanconlosestatutosdelserviciodeaseoObservacin`, `CuentanconlosestatutosdelserviciodealcantarilladoCumple`, `CuentanconlosestatutosdelserviciodealcantarilladoObservacin`, `CuentanconelregistromercantilCumple`, `CuentanconelregistromercantilMtododeverificacin`, `CuentanconelregistromercantilObservacin`, `CuentanconlaescriturapblicadeconstitucindelaasociacinCumple`, `CuentanconlaescriturapblicadeconstitucindelaasociacinObservacin`, `CuentanconelregistrodecmaradecomercioCumple`, `CuentanconelregistrodecmaradecomercioMtododeverificacin`, `CuentanconelregistrodecmaradecomercioObservacin`, `CuentanconoficinasdecontrolinternoCumple`, `CuentanconoficinasdecontrolinternoMtododeverificacin`, `CuentanconoficinasdecontrolinternoObservacin`, `ElMunicipiocuentaconloscontratosdeconcesionesvigentesCumple`, `ElMunicipiocuentaconloscontratosdeconcesionesvigentesObservacin`, `CuentanconlospermisosambientalesysanitariosrequeridosCumple`, `CuentanconlospermisosambientalesysanitariosrequeridosObservacin`, `PerteneceelmunicipioalPDACumple`, `PerteneceelmunicipioalPDAMtododeverificacin`, `PerteneceelmunicipioalPDAObservacin`, `CuentanconuncontrolsocialdeserviciospblicosCumple`, `CuentanconuncontrolsocialdeserviciospblicosMtododeverificacin`, `CuentanconuncontrolsocialdeserviciospblicosObservacin`, `RealizaronexpropiacionesparalaprestacindelservicioCumple`, `RealizaronexpropiacionesparalaprestacindelservicioObservacin`, `CuentanconuncontratodecondicionesuniformesCumple`, `CuentanconuncontratodecondicionesuniformesMtododeverificacin`, `CuentanconuncontratodecondicionesuniformesObservacin`, `EnlaactualidadlaempresacuentaconprocesosjudicialesvigentesCumple`, `CuentanconoficinadeatencinparaPQRSCumple`, `CuentanconoficinadeatencinparaPQRSMtododeverificacin`, `CuentanconoficinadeatencinparaPQRSObservacin`, `PersonasconcontratolaboralescritoAcueducto`, `PersonasconcontratolaboralescritoAlcantarillado`, `PersonasconcontratolaboralescritoAseo`, `PersonasconcontratodeprestacindeserviciosAcueducto`, `PersonasconcontratodeprestacindeserviciosAlcantarillado`, `PersonasconcontratodeprestacindeserviciosAseo`, `PersonaslaborandoAcueducto`, `PersonaslaborandoAlcantarillado`, `PersonaslaborandoAseo`, `Procesosadministrativosfalladosdesde2017Acueducto`, `Procesosadministrativosfalladosdesde2017Alcantarillado`, `Procesosadministrativosfalladosdesde2017Aseo`, `ProcesosadministrativosfalladosencontraAcueducto`, `ProcesosadministrativosfalladosencontraAlcantarillado`, `ProcesosadministrativosfalladosencontraAseo`, `Procesosadministrativosen2017Acueducto`, `Procesosadministrativosen2017Alcantarillado`, `Procesosadministrativosen2017Aseo`, `Procesosadministrativosen2018Acueducto`, `Procesosadministrativosen2018Alcantarillado`, `Procesosadministrativosen2018Aseo`, `Procesosjudicialesdesde2016Acueducto`, `Procesosjudicialesdesde2016Alcantarillado`, `Procesosjudicialesdesde2016Aseo`, `ProcesosjudicialesfalladosencontraAcueducto`, `ProcesosjudicialesfalladosencontraAlcantarillado`, `ProcesosjudicialesfalladosencontraAseo`, `TiempoderespuestapromedioAcueducto`, `TiempoderespuestapromedioAlcantarillado`, `TiempoderespuestapromedioAseo`, `PQRSrecibidasen2018Acueducto`, `PQRSrecibidasen2018Alcantarillado`, `PQRSrecibidasen2018Aseo`, `PQRSrespondidasenlostiemposdeleyen2018Acueducto`, `PQRSrespondidasenlostiemposdeleyen2018Alcantarillado`, `PQRSrespondidasenlostiemposdeleyen2018Aseo`)VALUES (".$gravityvalues.")";
        }
        else if($table_name == 'FormatodeDiagnosticoTcnicoLinea1'){
               echo  $sql="INSERT INTO `FormatodeDiagnosticoTcnicoLinea1` (`Departamentodeatencin`, `Municipiodeatencin`, `PDA`, `TipodeLocalidad`, `NombredelaLocalidad`, `Nmerototaldedomiciliosenlalocalidad`, `Tipodeentidadprestadora`, `Nombre`, `Direccin`, `TelfonoFax`, `Departamentodedomicilio`, `Municipiodedomicilio`, `NIT`, `Fechadeconstitucin`, `Representantelegal`, `Cargo`, `Correo`, `RUPS`, `NmerodeempleadosadministrativosAcueducto`, `NmerodeoperariosGeneralesAcueducto`, `NmerodecontratistasAcueducto`, `TotalnmerodeempleadosAcueducto`, `NmerodeempleadosadministrativosAlcantarillado`, `NmerodeoperariosGeneralesAlcantarillado`, `NmerodecontratistasAlcantarillado`, `TotalnmerodeempleadosAlcantarillado`, `NmerodeempleadosadministrativosAseo`, `NmerodeoperariosGeneralesAseo`, `NmerodeoperariosdepotabilizacindeaguasyfontanerosmunicipalesAseo`, `NmerodecontratistasAseo`, `TotalnmerodeempleadosAseo`, `SecuentaconconcesindeaguasCumple`, `SecuentaconconcesindeaguasMtododeverificacin`, `SecuentaconconcesindeaguasObservacin`, `LabocatomacuentaconrejillaCumple`, `LabocatomacuentaconrejillaMtododeverificacin`, `LabocatomacuentaconrejillaObservacin`, `LabocatomacuentacondesarenadorCumple`, `LabocatomacuentacondesarenadorMtododeverificacin`, `LabocatomacuentacondesarenadorObservacin`, `LabocatomacuentaconsistemadecontrolCumple`, `LabocatomacuentaconsistemadecontrolMtododeverificacin`, `LabocatomacuentaconsistemadecontrolObservacin`, `LabocatomacuentaconbombasCumple`, `LabocatomacuentaconbombasMtododeverificacin`, `LabocatomacuentaconbombasObservacin`, `ExistenactividadesantrpicasaguasarribadelabocatomaCumple`, `ExistenactividadesantrpicasaguasarribadelabocatomaNotas`, `ExistenactividadesantrpicasaguasarribadelabocatomaObservacin`, `CuentaconplantadetratamientodeaguapotableCumple`, `CuentaconplantadetratamientodeaguapotableMtododeverificacin`, `CuentaconplantadetratamientodeaguapotableObservacin`, `LaplantadetratamientocuentaconcmarademezclaCumple`, `LaplantadetratamientocuentaconcmarademezclaMtododeverificacin`, `LaplantadetratamientocuentaconcmarademezclaNotas`, `LaplantadetratamientocuentaconcmarademezclaObservacin`, `LaplantadetratamientocuentaconfloculadoresCumple`, `LaplantadetratamientocuentaconfloculadoresMtododeverificacin`, `LaplantadetratamientocuentaconfloculadoresObservacin`, `LaplantadetratamientocuentaconsedimentadoresCumple`, `LaplantadetratamientocuentaconsedimentadoresMtododeverificacin`, `LaplantadetratamientocuentaconsedimentadoresObservacin`, `LaplantadetratamientocuentaconfiltrosCumple`, `LaplantadetratamientocuentaconfiltrosMtododeverificacin`, `LaplantadetratamientocuentaconfiltrosObservacin`, `EnlaplantadetratamientoserealizaelprocesodedesinfeccinCumple`, `EnlaplantadetratamientoserealizaelprocesodedesinfeccinNotas`, `EnlaplantadetratamientoserealizaelprocesodedesinfeccinObservacin`, `LaplantadetratamientocuentaconbioalarmaCumple`, `LaplantadetratamientocuentaconbioalarmaMtododeverificacin`, `LaplantadetratamientocuentaconbioalarmaObservacin`, `LaplantadetratamientocuentacontanquesdealmacenamientoCumple`, `LaplantadetratamientocuentacontanquesdealmacenamientoObservacin`, `EnlaplantadetratamientosecuentaconunsistemademanejodelodosCumple`, `CuentalaplantaconplandecontigenciasCumple`, `CuentalaplantaconplandecontigenciasMtododeverificacin`, `CuentalaplantaconplandecontigenciasObservacin`, `LaplantadetratamientocuentaconmanualdeoperacionesCumple`, `LaplantadetratamientocuentaconmanualdeoperacionesObservacin`, `SecuentaconunplandereduccindeperdidasCumple`, `SecuentaconunplandereduccindeperdidasMtododeverificacin`, `SecuentaconunplandereduccindeperdidasObservacin`, `SerealizamedicindeparametrosdecalidaddelaguaenlaPTAPCumple`, `SerealizamedicindeparametrosdecalidaddelaguaenlaPTAPObservacin`, `SecuentaconPlandeusoeficienteyahorrodelAguaPUEAACumple`, `SecuentaconPlandeusoeficienteyahorrodelAguaPUEAAObservacin`, `LosoperariosdelaplantadetratamientoseencuentrancapacitadosCumple`, `ElmunicipiocuentaconsistemadealcantarilladoCumple`, `ElmunicipiocuentaconsistemadealcantarilladoMtododeverificacin`, `ElmunicipiocuentaconsistemadealcantarilladoObservacin`, `SecuentaconplandesaneamientoymanejodevertimientosPSMVCumple`, `SecuentaconplandesaneamientoymanejodevertimientosPSMVObservacin`, `SecuentaconplantadetratamientodeaguasresidualesCumple`, `SecuentaconplantadetratamientodeaguasresidualesObservacin`, `LaplantadetratamientocuentaconsistemadecribadoCumple`, `LaplantadetratamientocuentaconsistemadecribadoMtododeverificacin`, `LaplantadetratamientocuentaconsistemadecribadoObservacin`, `LaplantadetratamientocuentaconreactoresbiolgicosCumple`, `LaplantadetratamientocuentaconreactoresbiolgicosNotas`, `LaplantadetratamientocuentaconreactoresbiolgicosObservacin`, `LaplantadetratamientocuentaconbiodigestoresCumple`, `LaplantadetratamientocuentaconbiodigestoresMtododeverificacin`, `LaplantadetratamientocuentaconbiodigestoresObservacin`, `LaplantadetratamientocuentaconlechosdesecadosCumple`, `LaplantadetratamientocuentaconlechosdesecadosMtododeverificacin`, `LaplantadetratamientocuentaconlechosdesecadosObservacin`, `SecuentaconpermisodevertimientosdeaguasresidualesCumple`, `SecuentaconpermisodevertimientosdeaguasresidualesObservacin`, `SecuentaconmanualdeoperacindelaplantadetratamientoCumple`, `SecuentaconmanualdeoperacindelaplantadetratamientoObservacin`, `CuentanconvehculoscompactadoresparalarecoleccinderesiduosCumple`, `SecuentaconprogramasdereciclajeoasociacionesderecicladoresCumple`, `LaempresacuentaconrutasselectivasCumple`, `LaempresacuentaconrutasselectivasMtododeverificacin`, `LaempresacuentaconrutasselectivasObservacin`, `CuentanconPGRISactualizadoCumple`, `CuentanconPGRISactualizadoMtododeverificacin`, `CuentanconPGRISactualizadoObservacin`, `SeprestaelserviciodelimpiezadereaspblicasCumple`, `SeprestaelserviciodelimpiezadereaspblicasMtododeverificacin`, `SeprestaelserviciodelimpiezadereaspblicasNotas`, `SeprestaelserviciodelimpiezadereaspblicasObservacin`, `SecuentaconrellenosanitariomunicipalCumple`, `SecuentaconrellenosanitariomunicipalMtododeverificacin`, `SecuentaconrellenosanitariomunicipalObservacin`, `LadisposicinfinaldelosresiduosslidosessubcontradadaCumple`, `LadisposicinfinaldelosresiduosslidosessubcontradadaObservacin`, `SerealizaunmanejoadecuadodeloslixiviadosCumple`, `SerealizaunmanejoadecuadodeloslixiviadosMtododeverificacin`, `SerealizaunmanejoadecuadodeloslixiviadosObservacin`, `ElrellenosanitariocuentaconcontroldeoloresCumple`, `ElrellenosanitariocuentaconcontroldeoloresMtododeverificacin`, `ElrellenosanitariocuentaconcontroldeoloresObservacin`, `ElrellenosanitariocuentaconbsculaparaelpesajedelosresiduosCumple`, `ElrellenosanitariocuentaconlicenciaambientalCumple`, `ElrellenosanitariocuentaconlicenciaambientalMtododeverificacin`, `ElrellenosanitariocuentaconlicenciaambientalObservacin`, `LosoperariosdelrellenosanitarioseencuentrancapacitadosCumple`, `LosoperariosdelrellenosanitarioseencuentrancapacitadosObservacin`, `Nmerodesuscriptoresdeacueducto`, `Nmerototaldeviviendasodomicilios`, `Volumendeaguaproducida`, `Volumendeaguafacturada`, `Nmerodemicromedidoresinstalados`, `Nmerodemicromedidoresenfuncionamiento`, `Nmerodesuscriptoresdealcantarillado`, `Nmerodeviviendascontratamientodeaguasresiduales`, `Nmerodedomiciliosatendidosenrecoleccinytransportederesiduos`, `Dasqueseprestaelservicioderecoleccinytransportealao`, `Dasquefueprestadoelservicioenelltimoao`, `Capacidadinstaladadelrelleno`, `Capacidadutilizadadelrelleno`, `SehapresentadounainterrupcindelserviciodeAcueductoSiNo`, `ContinuidadAcueducto`, `Horastotalesdelao`, `Capacidadinstaladadelrellenosanitario`, `Vidatildelrellenosanitario`, `BasuradispuestaenelrellenosanitarioToneladas`, `BasuraproducidaToneladas`, `SehapresentadounainterrupcindelserviciodeAseoSiNo`, `ContinuidadAseo`, `Nmerodevecesenelaoenquesedebeprestarelserviciodeaseo`, `TotaldeusuariosatendidosconAseo`) VALUES (".$gravityvalues.")";
        }
        else if($table_name == 'FormatodediagnsticoTcnicoLnea2')
        {
            $sql="INSERT INTO `FormatodediagnsticoTcnicoLnea2` (`id`, `Departamentodeatencin`, `Municipiodeatencin`, `PDA`, `TipodeLocalidad`, `Nombredelalocalidad`, `Nmerototaldedomiciliosenlalocalidad`, `Tipodeentidadprestadora`, `Nombre`, `Direccin`, `TelfonoFax`, `Departamentodedomicilio`, `Municipiodedomicilio`, `NIT`, `Fechadeconstitucin`, `Representantelegal`, `Cargo`, `Correo`, `RUPS`, `Nmerodeempleadosadministrativos`, `Nmerooperariosgenerales`, `Nmerooperariosdepotabilizacindeaguasyfontanerosmunicipales`, `Contratistas`, `TotalnmerodeempleadosAcueducto`, `ExistenactividadeseconmicasaguasarribadelabocatomaCumple`, `ExistenactividadeseconmicasaguasarribadelabocatomaObservacin`, `LamicrocuencacuentaconcerramientoCumple`, `LamicrocuencacuentaconcerramientoMtododeverificacin`, `LamicrocuencacuentaconcerramientoObservacin`, `LabocatomacuentaconrejillaCumple`, `LabocatomacuentaconrejillaMtododeverificacin`, `LabocatomacuentaconrejillaObservacin`, `LabocatomacuentaconcajadederivacinCumple`, `LabocatomacuentaconcajadederivacinMtododeverificacin`, `LabocatomacuentaconcajadederivacinObservacin`, `LabocatomacuentaconsistemadecontrolCumple`, `LabocatomacuentaconsistemadecontrolMtododeverificacin`, `LabocatomacuentaconsistemadecontrolObservacin`, `LabocatomacuentacondesarenadorCumple`, `LabocatomacuentacondesarenadorMtododeverificacin`, `LabocatomacuentacondesarenadorObservacin`, `LabocatomacuentaconsistemadebombeoCumple`, `LabocatomacuentaconsistemadebombeoMtododeverificacin`, `LabocatomacuentaconsistemadebombeoObservacin`, `LabocatomacuentaconotrossistemasCumple`, `LabocatomacuentaconotrossistemasMtododeverificacin`, `LabocatomacuentaconotrossistemasObservacin`, `CuentaconpermisosdeconcesindeaguasCumple`, `CuentaconpermisosdeconcesindeaguasMtododeverificacin`, `CuentaconpermisosdeconcesindeaguasObservacin`, `CuentaconregistrosdelamedicindelcaudalCumple`, `CuentaconregistrosdelamedicindelcaudalMtododeverificacin`, `CuentaconregistrosdelamedicindelcaudalObservacin`, `EsposiblehacerlamedicindelcaudaldelafuenteCumple`, `EsposiblehacerlamedicindelcaudaldelafuenteMtododeverificacin`, `EsposiblehacerlamedicindelcaudaldelafuenteObservacin`, `LaplantadetratamientoesdesistemanoconvencionalCumple`, `LaplantadetratamientoesdesistemanoconvencionalMtododeverificacin`, `LaplantadetratamientoesdesistemanoconvencionalObservacin`, `LaplantadetratamientoesdesistemaconvencionalCumple`, `LaplantadetratamientoesdesistemaconvencionalMtododeverificacin`, `LaplantadetratamientoesdesistemaconvencionalObservacin`, `LaplantadetratamientocuentaconcmarademezclaCumple`, `LaplantadetratamientocuentaconcmarademezclaMtododeverificacin`, `LaplantadetratamientocuentaconcmarademezclaObservacin`, `LaplantadetratamientocuentaconfloculadorCumple`, `LaplantadetratamientocuentaconfloculadorMtododeverificacin`, `LaplantadetratamientocuentaconfloculadorObservacin`, `LaplantadetratamientocuentaconsedimentadorCumple`, `LaplantadetratamientocuentaconsedimentadorMtododeverificacin`, `LaplantadetratamientocuentaconsedimentadorObservacin`, `LaplantadetratamientocuentaconetapadefiltracinCumple`, `LaplantadetratamientocuentaconetapadefiltracinMtododeverificacin`, `LaplantadetratamientocuentaconetapadefiltracinObservacin`, `LaplantadetratamientocuentaconetapadedesinfeccinCumple`, `LaplantadetratamientocuentaconetapadedesinfeccinObservacin`, `LaplantadetratamientocuentaconestabilizacindelpHCumple`, `LaplantadetratamientocuentaconestabilizacindelpHObservacin`, `LaplantadetratamientocuentacontanquesdealmacenamientoCumple`, `LaplantadetratamientocuentacontanquesdealmacenamientoObservacin`, `LaplantadetratamientocuentaconsistemasdedeteccinycontrolCumple`, `SerealizamanejodelodosCumple`, `SerealizamanejodelodosMtododeverificacin`, `SerealizamanejodelodosObservacin`, `LaplantadetratamientocuentaconlechosdesecadoCumple`, `LaplantadetratamientocuentaconlechosdesecadoMtododeverificacin`, `LaplantadetratamientocuentaconlechosdesecadoObservacin`, `LaplantadetratamientocuentaconbioalarmaCumple`, `LaplantadetratamientocuentaconbioalarmaMtododeverificacin`, `LaplantadetratamientocuentaconbioalarmaObservacin`, `LaplantadetratamientocuentaconmacromedidorCumple`, `LaplantadetratamientocuentaconmacromedidorMtododeverificacin`, `LaplantadetratamientocuentaconmacromedidorObservacin`, `RealizaperidicamentemuestreoparaelanlisismicrobiolgicoCumple`, `RealizaperidicamentemuestreoparaelanlisismicrobiolgicoObservacin`, `CuentanconmapaderiesgosactualizadoCumple`, `CuentanconmapaderiesgosactualizadoMtododeverificacin`, `CuentanconmapaderiesgosactualizadoObservacin`, `CuentanconbitcoraolibrodenovedadesaldaCumple`, `CuentanconbitcoraolibrodenovedadesaldaMtododeverificacin`, `CuentanconbitcoraolibrodenovedadesaldaObservacin`, `CuentanconcertificadodecalibracindeequiposCumple`, `CuentanconcertificadodecalibracindeequiposMtododeverificacin`, `CuentanconcertificadodecalibracindeequiposObservacin`, `ElactadeinspeccinyconceptosanitarioseencuentraactualizadaCumple`, `TienenplandecontingenciaCumple`, `TienenplandecontingenciaMtododeverificacin`, `TienenplandecontingenciaObservacin`, `EstnregistradosenelSUICumple`, `EstnregistradosenelSUIMtododeverificacin`, `EstnregistradosenelSUIObservacin`, `LaentidadseencuentraaldaconlosreportesalSUICumple`, `LaentidadseencuentraaldaconlosreportesalSUIMtododeverificacin`, `LaentidadseencuentraaldaconlosreportesalSUIObservacin`, `TienenyestudianmetodologatarifariaCumple`, `TienenyestudianmetodologatarifariaMtododeverificacin`, `TienenyestudianmetodologatarifariaObservacin`, `CuentanconcontratodecondicionesuniformesCumple`, `CuentanconcontratodecondicionesuniformesMtododeverificacin`, `CuentanconcontratodecondicionesuniformesObservacin`, `Nmerodesuscriptores`, `Nmerodedomicilios`, `NmerooperarioscertificadosSENA`, `Nmerodeoperariosdelaempresa`, `Volumenproducido`, `Volumenfacturado`, `NmerodePQRSrecibidas`, `NmerodePQRSatendidas`, `SehapresentadounasuspensindelservicioSiNo`, `Continuidaddelservicio`) VALUES (".$gravityvalues.")";
        }
        else
        {
            echo $sql="INSERT INTO `$table_name` (".$capture_field.") VALUES (".$gravityvalues.")"; 
        }
           
        if($wpdb->get_var("SHOW TABLES LIKE 'metabase_$table_name'") != 'metabase_'.$table_name) {   
            echo $viewsql="CREATE VIEW `metabase_$table_name` AS SELECT * FROM `$table_name`";
            $wpdb->query($viewsql);
            
        }
        $wpdb->query($sql);
       
}